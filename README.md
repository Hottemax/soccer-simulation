<!---Test repo mirroring--->
* Simulation of FIFA World Cup with a simple Elo-based simulation system.
* Java with SWT user interface.
* Some German in the UI since it is for a family member (sorry!)

* Build with Apache Maven:
  * mvn install 
  * java -jar target/soccer-simulation-0.0.1-SNAPSHOT.jar
  
![Screenshot](screenshot.png)